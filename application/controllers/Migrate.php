<?php
if ( ! defined('BASEPATH')) exit("No direct script access allowed");

/**
 * Class Migrate
 */
class Migrate extends CI_Controller
{

	public function index()
	{
		$this->load->library('migration');

		if ($this->migration->current() === FALSE) {
			show_error($this->migration->error_string());
		}
	}

}
