<?php
if ( ! defined('BASEPATH')) exit("No direct script access allowed");

/**
 * Class Products
 */
class Products extends CI_Controller {

	/**
	 * Products constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('html');
		$this->load->helper('url_helper');
		$this->load->model('products_model');
		$this->load->model('categories_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('general_functions');
	}

	/**
	 * View a table that contains all the products from database
	 */
	public function index()
	{
		$data['title'] = 'Products';

		$this->setup_table();

		$this->load->view('templates/header', $data);
		$this->load->view('products/index', $data);
		$this->load->view('templates/footer');
	}

	/**
	 * Create a new product
	 */
	public function create()
	{
		$data['title'] = 'Add new product';
		$data['categories'] = $this->general_functions->generate_categories_dropdown($this->categories_model->get_categories());

		$this->setup_image();
		$this->set_rules_validation();

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header', $data);
			$this->load->view('products/create', $data);
			$this->load->view('templates/footer');
		} else {
			$this->products_model->set_product();
			redirect('/products', 'refresh');
		}
	}

	/**
	 * View the product
	 *
	 * @param $id
	 */
	public function view($id)
	{
		if ( !$id ) {
			redirect('/products', 'refresh');
		}

		$data['product'] = $this->products_model->get_product($id);

		$this->load->view('templates/header', $data);
		$this->load->view('products/view', $data);
		$this->load->view('templates/footer');
	}

	/**
	 * Edit a product
	 *
	 * @param $id
	 */
	public function edit($id)
	{
		if ( !$product = $this->products_model->get_product($id) ) {
			redirect('/products', 'refresh');
		}

		$data['title'] = 'Edit ' . $product['name'];
		$data['product'] = $product;
		$data['categories'] = $this->general_functions->generate_categories_dropdown($this->categories_model->get_categories());

		$this->setup_image();
		$this->set_rules_validation($product['barcode']);

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header', $data);
			$this->load->view('products/create', $data);
			$this->load->view('templates/footer');
		} else {
			$this->products_model->update_product($id);
			redirect('/products', 'refresh');
		}
	}

	/**
	 * Setup the table for products page
	 */
	protected function setup_table()
	{
		$this->load->library('table');
		$this->table->set_heading('Image', 'Name', 'Barcode', 'Category', 'Price Without Tax', 'Price With Tax');

		$products = $this->products_model->get_products();

		foreach ( $products as $product ) {
			$this->table->add_row(
				array(
					'<img src="' . ( $product['picture'] ? $product['picture'] : site_url('/public/images/no-image.png') ) . '" alt="product-image"/>',
					'<a href="'. site_url('/products/edit/' . $product['id']) .'">'. $product['name'] .'</a>',
					$product['barcode'],
					$product['category_name'],
					number_format($product['price_without_tax'] , 2, ',', '.') . ' RON',
					number_format($product['price_with_tax'] , 2, ',', '.') . ' RON'
				)
			);
		}

		$template = array(
			'table_open' => '<table id="products-table" class="table table-bordered table-hover">',
		);

		$this->table->set_template($template);
	}

	/**
	 * Set the rules for add/edit product form
	 *
	 * @param bool $original_barcode
	 */
	protected function set_rules_validation($original_barcode = false)
	{
		if ($this->input->post('barcode') != $original_barcode) {
			$is_unique = '|is_unique[products.barcode]';
		} else {
			$is_unique = '';
		}

		$this->form_validation->set_rules('name', 'product name', 'required');
		$this->form_validation->set_rules('category', 'product category', 'required');
		$this->form_validation->set_rules('barcode', 'barcode', 'required|exact_length[12]'.$is_unique);
		$this->form_validation->set_rules('price_without_tax', 'price without tax', 'required');
		$this->form_validation->set_rules('price_with_tax', 'price with tax', 'required');
		$this->form_validation->set_rules('tax_rate', 'tax rate', 'required');
		$this->form_validation->set_rules('description', 'product description', 'required');
	}

	/**
	 * Setup the product picture for upload
	 */
	protected function setup_image()
	{
		$config['upload_path']          = './public/uploads/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['file_ext_tolower']     = TRUE;
		$config['max_size']             = 100;
		$config['max_width']            = 2048;
		$config['max_height']           = 1536;

		$this->load->library('upload', $config);
	}
}
