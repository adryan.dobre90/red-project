<?php
if ( ! defined('BASEPATH')) exit("No direct script access allowed");

/**
 * Class Categories
 */
class Categories extends CI_Controller {

	/**
	 * Categories constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('html');
		$this->load->helper('text');
		$this->load->helper('url_helper');
		$this->load->library('form_validation');
		$this->load->library('general_functions');
		$this->load->model('categories_model');
		$this->load->model('products_model');
	}

	/**
	 * All categories page
	 */
	public function index()
	{
		$data['title'] = 'Categories';

		$this->setup_table();

		$this->load->view('templates/header', $data);
		$this->load->view('categories/index', $data);
		$this->load->view('templates/footer');
	}

	/**
	 * View and edit a category
	 *
	 * @param null $id
	 */
	public function view($id = NULL)
	{
		if ( !$category = $this->categories_model->get_category($id) ) {
			redirect('/categories', 'refresh');
		}

		$data['title'] = 'Edit '. $category['name'];
		$data['category'] = $category;
		$data['parent_categories'] = $this->get_parent_categories();

		$this->set_validations();

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header', $data);
			$this->load->view('categories/create', $data);
			$this->load->view('templates/footer');
		} else {
			$this->categories_model->update_category($id);
			redirect('/categories', 'refresh');
		}

	}

	/**
	 * Add new category
	 */
	public function create()
	{
		$data['title'] = 'Add new category';
		$data['parent_categories'] = $this->get_parent_categories();

		$this->set_validations();

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header', $data);
			$this->load->view('categories/create', $data);
			$this->load->view('templates/footer');
		} else {
			$this->categories_model->set_category();
			redirect('/categories', 'refresh');
		}
	}

	/**
	 * Setup the table for categories page
	 */
	protected function setup_table()
	{
		$this->load->library('table');
		$this->table->set_heading('Name', 'Parent Category', 'Description', 'Products No.');

		$categories = $this->categories_model->get_categories();

		foreach ( $categories as $category ) {
			$parent_category = $category['parent_id'] ? $this->categories_model->get_category($category['parent_id']) : NULL;
			$this->table->add_row(
				array(
					'<a href="'. base_url('/categories/view/' . $category['id']) .'">'. $category['name'] .'</a>',
					isset($parent_category['name']) && $parent_category['name'] ? $parent_category['name'] : NULL,
					word_limiter($category['description'], 10, '&#8230;'),
                    $this->products_model->get_products_count($category)
				)
			);
		}
		$template = array(
			'table_open' => '<table id="categories-table" class="table table-bordered table-hover">',
		);

		$this->table->set_template($template);
	}

	/**
	 * Get the parent categories for dropdown
	 *
	 * @return mixed
	 */
	protected function get_parent_categories()
	{
		return $this->general_functions->generate_dropdown($this->categories_model->get_categories(true), 'Select a category');
	}

	protected function set_validations()
	{
		$this->form_validation->set_rules('name', 'Category name', 'required');
		$this->form_validation->set_rules('description', 'Category description', 'required');
	}

}
