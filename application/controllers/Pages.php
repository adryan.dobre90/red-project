<?php
if ( ! defined('BASEPATH')) exit("No direct script access allowed");

/**
 * Class Pages
 */
class Pages extends CI_Controller {

	/**
	 * Pages constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('html');
		$this->load->helper('url_helper');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('general_functions');
		$this->load->model('pages_model');
		$this->load->model('categories_model');
		$this->load->model('products_model');
	}

	/**
	 * Home page
	 */
	public function index()
	{
		$this->load->library('migration');

		$this->migration->version(2);

		$data['categories'] = $this->prepare_categories_filter();
		$data['products'] = $this->products_model->get_products();

		$this->load->view('templates/header', $data);
		$this->load->view('pages/home', $data);
		$this->load->view('partial-views/products-list', $data, TRUE);
		$this->load->view('templates/footer', $data);
	}

    /**
     * Page of the categories with more than Y products with price higher than X
     */
	public function premium()
    {
        $data['categories'] = $this->prepare_premium_categories_filters();
        $data['products'] = $this->products_model->get_products(true);
        $data['premium'] = true;

        $this->load->view('templates/header', $data);
        $this->load->view('pages/home', $data);
        $this->load->view('partial-views/products-list', $data, TRUE);
        $this->load->view('templates/footer', $data);
    }

	/**
	 * Filter and order the products based on the category and/or the order selected
	 */
	public function filter()
	{
		$data['products'] = $this->products_model->get_products();

		$this->load->view('partial-views/products-list', $data);
	}

	/**
	 * Prepare the categories from the left filter
	 *
	 * @return array
	 */
	protected function prepare_categories_filter()
	{
		$result = array();
		$categories = $this->pages_model->get_categories_filters();

		// add to result the parent categories
		foreach ( $categories as $category ) {
			if ( !$category['is_parent'] ) {
				continue;
			}

			$category['products_count'] = $this->pages_model->get_product_count($category['id'], $category['is_parent']);
			$result[$category['id']] = $category;
		}

		// add to result the child categories grouped by parent category
		foreach ( $categories as $category ) {
			if ( $category['is_parent'] || !isset( $result[$category['parent_id']] ) ) {
				continue;
			}
			$result[$category['parent_id']]['categories'][$category['id']] = $category;
		}

		return $result;
	}

    /**
     * Prepare filters for categories with more than Y products with price higher than X
     *
     * @return array
     */
	protected function prepare_premium_categories_filters()
    {
        $result = array();
        $categories = $this->categories_model->get_premium_categories();

        // set parent categories
        foreach ( $categories as $category ) {
            if ( isset( $result[$category['parent_id']] ) ) {
                continue;
            }
            $result[$category['parent_id']]['id'] = $category['parent_id'];
            $result[$category['parent_id']]['name'] = $category['parent_name'];
            $result[$category['parent_id']]['products_count'] = 0;
        }

        // set subcategories
        foreach ( $categories as $category ) {
            if ( !isset( $result[$category['parent_id']] ) ) {
                continue;
            }
            $result[$category['parent_id']]['products_count'] = $result[$category['parent_id']]['products_count'] + $category['products_count'];
            $result[$category['parent_id']]['categories'][$category['id']] = $category;
        }

        return $result;
    }
}
