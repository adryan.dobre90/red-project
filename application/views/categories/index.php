<div class="container">
	<div class="row">
		<div class="col-12">
			<h1><?php echo $title ?></h1>
		</div>
		<div class="col-12">
			<div class="row justify-content-end">
				<div class="col-auto">
					<a href="<?php echo site_url('categories/create') ?>" class="btn red-button">Add new category</a>
				</div>
			</div>
		</div>
		<div class="col-12">
			<?php echo $this->table->generate(); ?>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready( function () {
        $('#categories-table').DataTable({
            "order": [
                [ 0, "asc" ]
            ],
            "columnDefs": [
                { "orderable": false, "targets": 1 },
                { "orderable": false, "targets": 2 }
            ]
        });
    } );
</script>
