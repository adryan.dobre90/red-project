<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="page-title text-center"><?php echo $title ?></h1>
		</div>
		<div class="col-12">
			<div class="row justify-content-center">
				<div class="col-12 col-md-6">
					<?php echo form_open('categories/' . ( isset ( $category ) ? 'view/' . $category['id'] : 'create' )); ?>
					<div class="form-group">
						<?php echo form_label('Category Name', 'category-name'); ?>
						<?php echo form_input(
							array(
								'name' => 'name',
								'id' => 'category-name',
								'class' => 'form-control',
								'autocomplete' => 'off'
							),
							set_value('name', isset($category) && $category['name'] ? $category['name'] : ''),
							''
						); ?>
						<?php echo form_error('name', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Category Description', 'category-description') ?>
						<?php echo form_textarea(
							array(
								'name' => 'description',
								'id' => 'category-description',
								'class' => 'form-control',
								'rows' => '5'
							),
							set_value('description', isset($category) && $category['description'] ? $category['description'] : ''),
							''
						); ?>
						<?php echo form_error('description', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Is Parent Category', 'is-parent') ?>
						<?php echo form_dropdown(
							'is_parent',
							array(
								'0' => 'No',
								'1' => 'Yes'
							),
							isset($category) && $category['is_parent'] ? $category['is_parent'] : $this->input->post('is_parent'),
							array(
								'id' => 'is-parent',
								'class' => 'form-control'
							)
						); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Parent Category', 'parent-category') ?>
						<?php echo form_dropdown(
							'parent_id',
							$parent_categories,
							isset($category) && $category['parent_id'] ? $category['parent_id'] : $this->input->post('parent_id'),
							array(
								'id' => 'parent-category',
								'class' => 'form-control'
							)
						); ?>
						<?php echo form_error('parent_id', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_submit(
							'submit',
							'Save',
							array(
								'class' => 'btn red-button'
							)
						) ?>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
