<?php if ( isset( $products ) && $products ) : ?>
	<?php foreach ( $products as $product ) : ?>
		<div class="col-12 col-md-4">
			<div class="product-card">
				<div class="product-image">
					<img src="<?php echo $product['picture'] ? $product['picture'] : site_url('/public/images/no-image.png') ?>" alt="product-image"/>
				</div>
				<div class="title text-center"><?php echo $product['name'] ?></div>
				<div class="text-center">
					<div class="product-price"><?php echo number_format($product['price_with_tax'], 2, ',', '.') . ' RON' ?></div>
				</div>
				<div class="text-center">
					<a class="btn red-button" href="<?php echo site_url('/products/view/' . $product['id']) ?>">View</a>
					<a class="btn red-button" href="<?php echo site_url('/products/edit/' . $product['id']) ?>">Edit</a>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
<?php else : ?>
	<div class="col-12">
		<strong>No results found!</strong>
	</div>
<?php endif ?>
