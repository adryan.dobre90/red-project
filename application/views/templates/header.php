<html>
<head>
	<?php echo meta('viewport', 'width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1, shrink-to-fit=no') ?>
	<?php echo meta('Content-type', 'text/html; charset=utf-8', 'equiv'); ?>
	<title>RedMusic Project</title>
	<script type="text/javascript" src="/public/libraries/jquery/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="/public/libraries/bootstrap/js/bootstrap.min.js"></script>
	<?php echo link_tag('/public/images/logo.png', 'shortcut icon', 'image/ico'); ?>
	<?php echo link_tag('https://fonts.googleapis.com/css?family=Arimo:400,700&display=swap', 'stylesheet', 'text/css'); ?>
	<?php echo link_tag('/public/libraries/bootstrap/css/bootstrap.min.css', 'stylesheet', 'text/css'); ?>
	<?php echo link_tag('/public/css/app.css', 'stylesheet', 'text/css'); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>

    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
</head>
<body>

<!-- start:Site Content -->
<div id="site-content">

	<!-- start:Header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url('/'); ?>">
					<img class="logo" src="/public/images/logo.png" alt="logo"/>
				</a>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="<?php echo site_url('premium'); ?>">Premium Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo site_url('categories'); ?>">Manage Categories</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo site_url('products'); ?>">Manage Products</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- end:Header -->

	<!-- start:Page Content -->
	<div id="page-content">

