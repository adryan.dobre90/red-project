</div>
<!-- end:Page Content -->

<!-- start:Footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="copyright text-center">&copy; <?php echo date('Y') ?></div>
			</div>
		</div>
	</div>
</footer>
<!-- end:Footer -->

</div>
<!-- end:Site Content -->

</body>
</html>
