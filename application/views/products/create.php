<div class="container">
    <?php
    if ( isset($product) && $product['id'] ) {
        echo form_open_multipart('products/edit/' . $product['id']);
    } else {
        echo form_open_multipart('products/create');
    }
    ?>
	<div class="row justify-content-center">
		<div class="col-12">
			<h1 class="page-title text-center"><?php echo $title ?></h1>
		</div>
		<div class="col-12">
			<div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <div class="product-image-wrapper">
                        <img src="<?php echo isset($product) && $product['picture'] ? $product['picture'] : base_url('public/images/no-image.png') ?>"/>
                    </div>
                    <div class="form-group pt-md-3">
                        <div class="upload-button-wrap">
                            <div class="btn upload-btn red-button">Upload image</div>
                            <?php echo form_upload(
                                array(
                                    'id' => 'product-upload-image',
                                    'name' => 'picture',
                                    'class' => 'hidden'
                                )
                            ) ?>
                        </div>
                        <?php echo form_error('picture', '<div class="error">', '</div>'); ?>
                    </div>
                </div>
				<div class="col-12 col-md-6">
					<div class="form-group">
						<?php echo form_label('Product Name', 'product-name'); ?>
						<?php echo form_input(
							array(
								'name' => 'name',
								'id' => 'product-name',
								'class' => 'form-control',
								'autocomplete' => 'off'
							),
							set_value('name', isset($product) && $product['name'] ? $product['name'] : '', false),
							''
						); ?>
						<?php echo form_error('name', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Category', 'category') ?>
						<?php echo form_dropdown(
							array(
								'name' => 'category',
								'id' => 'category',
								'class' => 'form-control'
							),
							$categories,
							isset($product) && $product['category'] ? $product['category'] : $this->input->post('category')
						); ?>
						<?php echo form_error('category', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Barcode', 'product-barcode'); ?>
						<?php echo form_input(
							array(
								'name' => 'barcode',
								'id' => 'product-barcode',
								'class' => 'form-control',
								'autocomplete' => 'off'
							),
							set_value('barcode', isset($product) && $product['barcode'] ? $product['barcode'] : ''),
							''
						); ?>
						<?php echo form_error('barcode', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Second Barcode', 'product-second-barcode'); ?>
						<?php echo form_input(
							array(
								'name' => 'second_barcode',
								'id' => 'product-second-barcode',
								'class' => 'form-control',
								'autocomplete' => 'off'
							),
							set_value('second_barcode', isset($product) && $product['second_barcode'] ? $product['second_barcode'] : ''),
							''
						); ?>
						<?php echo form_error('second_barcode', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Price Without Tax', 'price-without-tax'); ?>
						<?php echo form_input(
							array(
								'name' => 'price_without_tax',
								'id' => 'price-without-tax',
								'class' => 'form-control',
								'autocomplete' => 'off'
							),
							set_value(
								'price_without_tax',
								isset($product) && $product['price_without_tax'] ? number_format($product['price_without_tax'], 2, ',', '.') : ''
							),
							''
						); ?>
						<?php echo form_error('price_without_tax', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Price With Tax', 'price-with-tax'); ?>
						<?php echo form_input(
							array(
								'name' => 'price_with_tax',
								'id' => 'price-with-tax',
								'class' => 'form-control',
								'autocomplete' => 'off'
							),
							set_value(
								'price_with_tax',
								isset($product) && $product['price_with_tax'] ? number_format($product['price_with_tax'], 2, ',', '.') : ''
							),
							''
						); ?>
						<?php echo form_error('price_with_tax', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Tax Rate', 'tax-rate'); ?>
						<?php echo form_input(
							array(
								'name' => 'tax_rate',
								'id' => 'tax-rate',
								'class' => 'form-control',
								'autocomplete' => 'off'
							),
							set_value(
								'tax_rate',
								isset($product) && $product['tax_rate'] ? number_format($product['tax_rate'], 2, ',', '.') : ''
							),
							''
						); ?>
						<?php echo form_error('tax_rate', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Product Description', 'product-description') ?>
						<?php echo form_textarea(
							array(
								'name' => 'description',
								'id' => 'product-description',
								'class' => 'form-control',
								'rows' => '5'
							),
							set_value('description', isset($product) && $product['description'] ? $product['description'] : ''),
							''
						); ?>
						<?php echo form_error('description', '<div class="error">', '</div>'); ?>
					</div>
				</div>
                <div class="col-12 col-md-4 pt-md-5">
                    <div class="form-group">
                        <?php echo form_submit(
                            'submit',
                            'Save',
                            array(
                                'class' => 'btn red-button'
                            )
                        ) ?>
                    </div>
                </div>
			</div>
		</div>
	</div>
    </form>
</div>

<script type="text/javascript">
    $(document).on('change', '#product-upload-image', function (event) {
        $('.product-image-wrapper img').attr('src', URL.createObjectURL(event.target.files[0]));
    })
</script>