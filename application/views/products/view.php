<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="page-title"><?php echo $product['name'] ?></h1>
			<p><strong>Barcode:</strong> <?php echo $product['barcode'] ?></p>
		</div>
		<div class="col-12 col-md-6">
			<div class="">
				<img src="<?php echo $product['picture'] ? $product['picture'] : site_url('/public/images/no-image.png') ?>" alt="product-image"/>
			</div>
		</div>
		<div class="col-12 col-md-6">
			<div><strong>Description:</strong></div>
			<div><?php echo $product['description'] ?></div>
			<div class="product-category"><strong>Category:</strong> <?php echo $product['category_name'] ?></div>
			<div class="product-price"><strong>Price:</strong> <?php echo number_format($product['price_with_tax'], 2, ',', '.') . ' RON' ?></div>
		</div>
	</div>
</div>
