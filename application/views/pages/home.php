<div class="container">
	<div class="row">
		<div class="col-12">
			<?php echo form_open('pages/filter/', array('id' => 'products-filter')); ?>
			<?php echo form_hidden('category', ''); ?>
			<?php echo form_hidden('is_parent', false); ?>
            <?php
            if ( isset($premium) && $premium ) {
                echo form_hidden('is_premium', true);
            }
            ?>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-12 col-md-3">
					<div class="form-group">
						<?php echo form_label('Active category', '', array('class' => 'hidden')) ?>
						<div class="active-category"></div>
					</div>
				</div>
				<div class="col-12 col-md-3">
					<div class="form-group">
						<?php echo form_label('Order By', 'order-by-filter') ?>
						<?php echo form_dropdown(
							array(
								'name' => 'order-by',
								'id' => 'order-by-filter',
								'class' => 'form-control'
							),
							array(
								'name' => 'Name',
								'price_with_tax' => 'Price'
							),
							''
						); ?>
					</div>
				</div>
				<div class="col-12 col-md-3">
					<div class="form-group">
						<?php echo form_label('Order', 'order-filter') ?>
						<?php echo form_dropdown(
							array(
								'name' => 'order',
								'id' => 'order-filter',
								'class' => 'form-control'
							),
							array(
								'asc' => 'Ascendent',
								'desc' => 'Descendent'
							),
							''
						); ?>
					</div>
				</div>
			</div>
			</form>
		</div>
		<div class="col-12 col-md-3">
			<ul class="list-group">
				<?php foreach ( $categories as $category ) : ?>
					<li class="list-group-item">
						<div class="parent-category d-flex justify-content-between align-items-center">
							<span class="pick-category" data-id="<?php echo $category['id'] ?>" data-is-parent="true"><?php echo $category['name'] ?></span>
							<span class="badge badge-primary badge-pill"><?php echo $category['products_count'] ?></span>
						</div>
						<?php if ( isset($category['categories']) && $category['categories'] ) : ?>
							<div class="subcategories">
								<ul>
									<?php foreach ( $category['categories'] as $subcategory ) : ?>
										<li class="d-flex justify-content-between align-items-center">
											<span class="pick-category" data-id="<?php echo $subcategory['id'] ?>"><?php echo $subcategory['name'] ?></span>
											<span class="badge badge-primary badge-pill"><?php echo $subcategory['products_count'] ?></span>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
						<?php endif ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="col-12 col-md-9">
			<div class="row">

				<div class="col-12">
					<div class="products-list row">
						<?php $this->load->view("partial-views/products-list"); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('click', '.pick-category', function () {
			let self = $(this);
			let is_parent = false;
			if ( self.attr('data-is-parent') == 'true' ) {
				is_parent = true;
			}
			$('input[name="is_parent"]').val(is_parent);
			$('input[name="category"]').val(self.attr('data-id'));
			$('.active-category').html(self.text());
			$('#products-filter').submit();
		});

		$(document).on('change', '#products-filter .form-control', function () {
			$('#products-filter').submit();
		});

		$("#products-filter").submit(function(e) {
			e.preventDefault();
			$.ajax({
				url: '<?php echo base_url('/pages/filter/'); ?>',
				type: 'POST',
				data: $('#products-filter').serialize(),
				dataType: "html",
				success: function(response){
					$('.products-list').html(response);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}
			});
		});
	});
</script>
