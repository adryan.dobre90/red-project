<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class General_Functions
 */
class General_Functions {

	/**
	 * Generate dropdown data
	 *
	 * @param $data
	 * @param string $default
	 * @return array|string
	 */
	public function generate_dropdown($data, $default = '')
	{
		if ( empty($data) ) {
			return 'No data available';
		}

		$result = array();
		$result[0] = $default;

		foreach ( $data as $row ) {
			$result[$row['id']] = $row['name'];
		}

		return $result;
	}

	/**
	 * Generate a dropdown of categories and subcategories
	 *
	 * @param $categories
	 * @return array|string
	 */
	public function generate_categories_dropdown($categories)
	{
		if ( empty($categories) ) {
			return 'No category available';
		}

		$result = array();
		$parent_categories = array();
		$result[NULL] = 'Select a category';

		foreach ( $categories as $category ) {
			if ( !$category['is_parent'] ) {
				continue;
			}
			$parent_categories[$category['id']] = $category['name'];
		}

		foreach ( $categories as $category ) {
			if ( $category['is_parent'] ) {
				continue;
			}
			$result[$parent_categories[$category['parent_id']]][$category['id']] = $category['name'];
		}

		ksort($result);

		return $result;
	}

	/**
	 * Change the number format before saving into database
	 *
	 * @param $number
	 * @return string|string[]
	 */
	public function custom_format($number)
	{
		$number = str_replace('.', '', $number);
		$number = str_replace(',', '.', $number);

		return $number;
	}
}
