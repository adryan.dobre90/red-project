<?php

/**
 * Class Migration_Add_user
 */
class Migration_Add_products extends CI_Migration
{
	public function up()
	{
		$this->dbforge->add_field(
			array(
				'id' => array(
					'type' => 'INT',
					'constraint' => 11,
					'unsigned' => true,
					'auto_increment' => true
				),
				'name' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
				),
				'category' => array(
					'type' => 'INT',
					'constraint' => '11'
				),
				'barcode' => array(
					'type' => 'VARCHAR',
					'constraint' => '12',
				),
				'second_barcode' => array(
					'type' => 'VARCHAR',
					'constraint' => '12',
					'null' => TRUE
				),
				'price_without_tax' => array(
					'type' => 'DECIMAL',
					'constraint' => '10,2'
				),
				'price_with_tax' => array(
					'type' => 'DECIMAL',
					'constraint' => '10,2'
				),
				'tex_rate' => array(
					'type' => 'DECIMAL',
					'constraint' => '10,2'
				),
				'description' => array(
					'type' => 'TEXT'
				),
				'picture' => array(
					'type' => 'TEXT',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME'
				),
				'updated_at' => array(
					'type' => 'DATETIME'
				),
				'deleted_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
			)
		);

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('products');
	}

	public function down()
	{
		$this->dbforge->drop_table('products');
	}
}
