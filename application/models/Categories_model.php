<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Categories_model
 */
class Categories_model extends CI_Model {

	/**
	 * Categories_model constructor.
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('url');
	}

	/**
	 * Get the category based on the category id
	 *
	 * @param $id
	 * @return mixed
	 */
	public function get_category($id)
	{
		$query = $this->db->get_where('categories', array('id' => $id));

		return $query->row_array();
	}

	/**
	 * Get get all categories from db
	 *
	 * @param bool $is_parent
	 * @return mixed
	 */
	public function get_categories($is_parent = false)
	{
		if ($is_parent)
		{
			$this->db->order_by('name', 'ASC');
			$query = $this->db->get_where('categories', array('is_parent' => 1, 'deleted_at' => NULL));

			return $query->result_array();
		}

		$this->db->order_by('name', 'ASC');
		$query = $this->db->get_where('categories', array('deleted_at' => NULL));

		return $query->result_array();
	}

	/**
	 * Insert into database a new category
	 *
	 * @param bool $data
	 * @return mixed
	 */
	public function set_category($data = false)
	{
		if ( !$data ) {
			$data = $this->prepare_data();
		}

		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');

		return $this->db->insert('categories', $data);
	}

	/**
	 * Update a category from database
	 *
	 * @param $id
	 * @return mixed
	 */
	public function update_category($id)
	{
		$data = $this->prepare_data();
		$data['updated_at'] = date('Y-m-d H:i:s');

		$this->db->where('id', $id);
		return $this->db->update('categories', $data);
	}

    /**
     * Get the categories with more than Y products with price higher than X
     *
     * @return mixed
     */
	public function get_premium_categories()
	{
		$query = $this->db->select('c.*, c2.name as parent_name, COUNT(p.id) as products_count')
			->from('categories c')
            ->join('categories c2', 'c.parent_id = c2.id', 'left')
			->join('products p', 'p.category = c.id', 'left')
			->where('p.price_with_tax >= '. Products_model::PREMIUM_MIN_PRICE . ' AND c.deleted_at IS NULL')
            ->group_by('c.id')
            ->having('COUNT(*) >= ' . Products_model::PREMIUM_MIN_PRODUCTS)
			->get();

		return $query->result_array();
	}

	/**
	 * Prepare data before insert/data
	 *
	 * @return array
	 */
	protected function prepare_data()
	{
		return array(
			'name' => $this->input->post('name'),
			'description' => $this->input->post('description'),
			'is_parent' => $this->input->post('is_parent') ? $this->input->post('is_parent') : 0,
			'parent_id' => $this->input->post('parent_id') || $this->input->post('parent_id') == 0 ? $this->input->post('parent_id') : NULL,
		);
	}

}
