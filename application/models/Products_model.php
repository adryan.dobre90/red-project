<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Products_model
 */
class Products_model extends CI_Model {

    const PREMIUM_MIN_PRICE = 300.00;
    const PREMIUM_MIN_PRODUCTS = 2;

	/**
	 * Products_model constructor.
	 */
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('general_functions');
	}

	/**
	 * Get a product by id
	 *
	 * @param $id
	 * @return mixed
	 */
	public function get_product($id)
	{
		$this->db->select('products.*, categories.name as category_name')
			->from('products')
			->join('categories', 'products.category = categories.id', 'left')
			->where(array('products.id' => $id, 'products.deleted_at' => NULL));
		$query = $this->db->get();

		return $query->row_array();
	}

    /**
     * Get all products that are not deleted
     *
     * @param bool $is_premium
     * @return mixed
     */
	public function get_products($is_premium = false)
	{
		$filters = $this->get_filter_data();

        if ( $is_premium || $filters['premium'] ) {
            $query2 = $this->db->select('c.id')
                ->from('categories c')
                ->join('products p', 'p.category = c.id', 'left')
                ->where('p.price_with_tax >= '.self::PREMIUM_MIN_PRICE.' AND c.deleted_at IS NULL GROUP BY c.id HAVING COUNT(*) >= '. self::PREMIUM_MIN_PRODUCTS)
                ->get();
            $cats = $query2->result_array();
            $_cat = array();
            foreach ( $cats as $cat ) {
                $_cat[] = $cat['id'];
            }
        }

		$this->db->select('products.*, categories.name as category_name');
		$this->db->from('products');
		$this->db->join('categories', 'categories.id = products.category');

		if ( $filters['category'] && $filters['is_parent'] ) {
			$this->db->where('products.deleted_at IS NULL AND products.category IN (SELECT t.id FROM categories t WHERE t.parent_id = '. $filters['category'] .')');
		} elseif ( $filters['category'] && !$filters['is_parent'] ) {
			$this->db->where(array('products.category' => $filters['category']));
		}
        if ( $is_premium || $filters['premium'] ) {
            $this->db->where('products.category IN ('. implode(',', $_cat) . ') AND products.price_with_tax >= '. self::PREMIUM_MIN_PRICE );
        }

        $this->db->where(array('products.deleted_at' => NULL));
		$this->db->order_by('products.' . $filters['order_by'], $filters['order']);
		$query = $this->db->get();

		return $query->result_array();
	}

    /**
     * Add a new product into database
     *
     * @param bool $data
     * @return mixed
     */
	public function set_product($data = false)
	{
		if ( !$data ) {
            $data = $this->prepare_data();
        }

		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');

		return $this->db->insert('products', $data);
	}

	/**
	 * Update a product from database
	 *
	 * @param $id
	 * @return mixed
	 */
	public function update_product($id)
	{
		$data = $this->prepare_data();
		$data['updated_at'] = date('Y-m-d H:i:s');

		$this->db->where('id', $id);
		return $this->db->update('products', $data);
	}

    /**
     * Get products count from a specific category
     *
     * @param $category
     * @return mixed
     */
    public function get_products_count($category)
    {
        $this->db->select('COUNT(products.id) as count');
        $this->db->from('products');

        if ( $category['is_parent'] ) {
            $this->db->join('categories', 'categories.id = products.category', 'left');
            $this->db->where('categories.parent_id = ' . $category['id']);
        } else {
            $this->db->where('products.category = ' . $category['id']);
        }

        $this->db->where('products.deleted_at IS NULL');
        $query = $this->db->get();

        return $query->row()->count;
    }

	/**
	 * Prepare data before insert/update query
	 *
	 * @return array
	 */
	private function prepare_data()
	{
		$data =  array(
			'name' => $this->input->post('name'),
			'category' => $this->input->post('category'),
			'barcode' => $this->input->post('barcode'),
			'second_barcode' => $this->input->post('second_barcode') ? $this->input->post('second_barcode') : NULL,
			'price_without_tax' => $this->general_functions->custom_format($this->input->post('price_without_tax')),
			'price_with_tax' => $this->general_functions->custom_format($this->input->post('price_with_tax')),
			'tax_rate' => $this->general_functions->custom_format($this->input->post('tax_rate')),
			'description' => $this->input->post('description')
		);

		if ( ! empty($_FILES['picture']['name'])) {
			if ( $this->upload->do_upload('picture') ) {
				$data['picture'] = base_url('/public/uploads/' . $this->upload->data('file_name'));
			}
		}

		return $data;
	}

    /**
     * Prepare data for filtering products
     *
     * @return array
     */
	private function get_filter_data()
    {
        $filters = array();
        $filters['premium'] = $this->input->post('is_premium') ? true : false;
        $filters['category'] = $this->input->post('category') ? $this->input->post('category') : false;
        $filters['is_parent'] = $this->input->post('is_parent') && $this->input->post('is_parent') == 'true' ? true : false;
        $filters['order_by'] = $this->input->post('order-by') ? $this->input->post('order-by') : 'name';
        $filters['order'] = $this->input->post('order') ? $this->input->post('order') : 'ASC';

        return $filters;
    }

}

