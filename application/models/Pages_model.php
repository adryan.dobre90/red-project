<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Pages_model
 */
class Pages_model extends CI_Model {

	/**
	 * Pages_model constructor.
	 */
	public function __construct()
	{
		$this->load->database();
	}

	public function get_categories_filters()
	{
		$query = $this->db->select('categories.*, COUNT(products.id) as products_count')
			->from('categories')
			->join('products', 'products.category = categories.id AND products.deleted_at IS NULL', 'left')
			->where('categories.deleted_at', NULL)
			->group_by('categories.id')
			->get();

		return $query->result_array();
	}

    /**
     * Get the count of products from parent categories
     *
     * @param $id
     * @param bool $is_parent
     * @return mixed
     */
	public function get_product_count($id, $is_parent = false)
	{
		if ( $is_parent ) {
			$query = $this->db->select('COUNT(products.id) as products_count')
				->from('products')
				->join('categories', 'categories.id = products.category AND categories.deleted_at IS NULL', 'left')
				->where(array('products.deleted_at' => NULL, 'categories.parent_id' => $id))
				->get();

			return $query->row()->products_count;
		}

	}

}
